# Deployment

## Dependencies
- https://code.europa.eu/digit-c4/netbox - This repository depends on it to retrieve the data from netbox.

## Schema
![Service Installation Schema](docs/diagram/output/containerDeployment.png)
[Smithy Explication](docs/smithy)

## Playbook Structure
- ansible/playbooks/data_validation.yml - This will run the role data_validation and verify if the provided data is valid.
- ansible/playbooks/deploy.yml - This will deploy the container on the provided virtual machine


AWX - Setup - Workflow:
- Netbox : https://code.europa.eu/digit-c4/netbox.git : Dynamic Inventory
- Deployment : Project Sync
- Deployment - Validate Json Schema : data_validation.yml : Validate data with json Schema
- Deployment - Deploy Container : deploy.yml : Deploy the container


