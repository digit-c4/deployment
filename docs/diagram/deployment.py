from diagrams import Diagram
from diagrams.c4 import Person, Container, Database, System, Relationship, SystemBoundary

graph_attr = {
    "splines": "spline",
}

with Diagram("Container Deployment diagram", direction="TB", filename="output/containerDeployment", show=False, graph_attr=graph_attr):
    customer = Person(
        name="SNET User", description="A customer of the sys service for container deployment"
    )

    netbox = Container(
        name="Netbox",
        technology="Netbox on docker",
        description="The digit C4 source of thruth"
    )

    with SystemBoundary("AWX Workflow"):
        awx = Container(
            name="Deployment",
            technology="Awx Workflow",
            description="The Deployment workflow",
            width="4", 
            height="4"
        )

        awx_inventory_download = System(
            name="Netbox",
            technology="ansible",
            description="https://code.europa.eu/digit-c4/netbox.git - Global dynamic inventory to retrieve hosts from netbox",
            width="4",
            height="4"
        )

        awx_project_sync = System(
            name="Deployment",
            technology="ansible",
            description="https://code.europa.eu/digit-c4/deployment.git - Sync the project to awx",
            width="4",
            height="4"
        )

        awx_jsonschema_validation = System(
            name="Deployment - Validate Json Schema",
            technology="ansible",
            description="https://code.europa.eu/digit-c4/deployment/-/blob/main/ansible/playbooks/data_validation.yml - Jsonschema Data Validation",
            width="4",
            height="4"
        )

        awx_deploy_container = System(
            name="Deployment - Deploy container",
            technology="ansible",
            description="https://code.europa.eu/digit-c4/deployment/-/blob/main/ansible/playbooks/deploy.yml - Deploy Container on VM",
            width="4",
            height="4"
        )

    gitlab = System(
        name="Gitlab",
        technology="Version Control Software",
        description="The version control software which hosts the playbooks"
    )

    virtual_machine = System(
        name="Virtual Machine",
        technology="Debian12 / Docker",
        description="A virtual machine",
        external=True
    )

    customer >> Relationship("Visits Netbox and create a new service based on the VM") >> netbox
    netbox >> Relationship("Starts a webhook call to awx") >> awx
    awx >> awx_inventory_download >> Relationship("Download inventory Data from cmdb") >> netbox
    awx >> awx_project_sync >> Relationship("Downloads playbooks from gitlab") >> gitlab
    awx >> Relationship("Validate json from webhook input") >> awx_jsonschema_validation
    awx >> awx_deploy_container >> Relationship("Connects to virtual and deploys the docker images of the service") >> virtual_machine
