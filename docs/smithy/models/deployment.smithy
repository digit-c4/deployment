$version: "2"

namespace SysServiceCatalogue

use aws.protocols#restJson1

@restJson1
service Deployment {
    version: "0.0.1"
    operations: [DeployContainer]
}

@http(method: "POST", uri: "/DeployContainer")
operation DeployContainer {
    input: DeploymentInput
}

structure DeploymentInput {
    @required
    compose : Document
    // schema : https://raw.githubusercontent.com/compose-spec/compose-spec/master/schema/compose-spec.json#

    registry_credentials : RegistryCredentials

    @required
    vm : Vm

    @required
    service : String

    deployment_user : String = "admin"
    deployment_group : String = "admin"
    destination_prefix : String = "/opt/admin"
}

structure RegistryCredentialsStruct {
    @required
    username : String

    @required
    password : String 

    @required
    registry_url : String
}

list RegistryCredentials {
    member : RegistryCredentialsStruct
}

list Vm {
    member : String
}

